package com.wlmx.api.job;

/**
 * Тип задания
 */
public enum JobType {

    /**
     * Импорт данных с файловой системы в БД
     */
    IMPORT_FROM_FILE(Source.FILE, Source.DATABASE),

    /**
     * Экспорт данных из БД в файлововую систему
     */
    EXPORT_TO_FILE(Source.DATABASE, Source.FILE);

    /**
     * Исходный источник
     */
    private Source source;

    /**
     * Цель
     */
    private Source target;

    JobType(Source source, Source target) {
        this.source = source;
        this.target = target;
    }

    /**
     * Тип источника
     */
    public enum Source {
        /**
         * Файловая система
         */
        FILE,
        /**
         * База данных
         */
        DATABASE
    }
}