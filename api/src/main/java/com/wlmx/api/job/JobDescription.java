package com.wlmx.api.job;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Описание задания
 */
public class JobDescription implements Serializable {

    /**
     * Тип задания
     */
    private JobType type;

    /**
     * Свойства
     */
    private Map<String, String> properties = new HashMap<>();

    public Map<String, String> getProperties() {
        return properties;
    }

    public JobType getType() {
        return type;
    }

    public void setType(JobType type) {
        this.type = type;
    }
}
