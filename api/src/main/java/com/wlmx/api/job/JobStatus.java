package com.wlmx.api.job;

/**
 * Статус задания
 */
public enum JobStatus {

    /**
     * Зарегистрировано
     */
    REGISTERED,
    /**
     * Запущено
     */
    STARTED,
    /**
     * Закончено
     */
    ENDED,
    /**
     * Ошибка
     */
    ERROR;
}
