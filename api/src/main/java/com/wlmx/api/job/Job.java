package com.wlmx.api.job;

import java.io.Serializable;
import java.util.Date;

/**
 * Задание
 */
public class Job implements Serializable {

    /**
     * Идентификатор
     */
    private String id;

    /**
     * Дата и время запуска
     */
    private Date startTime;

    /**
     * Статус задания
     */
    private JobStatus status;

    /**
     * Описание задания
     */
    private JobDescription description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public JobDescription getDescription() {
        return description;
    }

    public void setDescription(JobDescription description) {
        this.description = description;
    }
}
