package com.wlmx.integration;

import com.wlmx.api.job.JobProperties;
import com.wlmx.api.job.service.JobService;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;

/**
 * Сервис обработки заданий
 */
@MessageEndpoint
public class JobServiceActivator implements JobService {

    @ServiceActivator
    public String startNewJob(String jobName, JobProperties properties) {
        return null;
    }
}
