package com.wlmx.api.job.service;

import com.wlmx.api.job.JobProperties;


/**
 * Сервис "Задание"
 */
public interface JobService {

    /**
     * Запуск нового задания
     * @param jobName наиеменование задания
     * @param properties параметры задания для выполнения
     * @return идентификатор задания
     */
    String startNewJob(String jobName, JobProperties properties);
}
