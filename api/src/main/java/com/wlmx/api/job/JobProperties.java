package com.wlmx.api.job;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Свойства задания
 */
public class JobProperties implements Serializable {

    private Map<String, Serializable> propertiesMap = new HashMap<>();

    /**
     * Проставление значения свойства
     * @param propertyName наименование свойства
     * @param value значение
     * @return предидущее значение
     */
    public Serializable set(String propertyName, Serializable value) {
        return this.propertiesMap.put(propertyName, value);
    }

    /**
     * Получение значения свойства
     * @param propertyName наименование свойства
     * @return значение
     */
    public Serializable get(String propertyName) {
        return this.propertiesMap.get(propertyName);
    }

    public enum Property {
        FILENAME,
        DATABASENAME,
        FILENAMEPATTERN
    }
}
